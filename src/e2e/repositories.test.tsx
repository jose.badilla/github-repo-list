import { act, cleanup, fireEvent, render, screen } from '@testing-library/react'
import React from 'react'
import App from '../components/app'
import { LocalStorageApi } from '../lib/services'
const BASE_GITHUB_URL = 'https://github.com'
describe('e2e', () => {
  jest.retryTimes(2)

  beforeAll(() => {
    cleanup()
    jest.useFakeTimers()
  })
  afterEach(cleanup)

  test('render app and check that repositories rendered', async () => {
    await act(async () => {
      render(<App />)
    })
    act(() => {
      jest.runAllTimers()
    })
    const cells = await screen.findAllByText(BASE_GITHUB_URL, { exact: false })
    cells.forEach((cell) => {
      expect(cell.closest('tr')).toBeDefined()
      expect(cell.closest('tr')!.firstChild).toBeDefined()
      expect(cell.closest('tr')!.firstChild!.textContent).toBeDefined()
      const projectName = cell.closest('tr')!.firstChild!.textContent
      expect(
        cell.closest('tr')!.firstChild!.nextSibling!.textContent
      ).toContain(BASE_GITHUB_URL)
      expect(
        cell.closest('tr')!.firstChild!.nextSibling!.textContent
      ).toContain(`/${projectName}`)
    })
  })

  test('e2e - render app, star repo and check local storage', async () => {
    await act(async () => {
      render(<App />)
    })
    act(() => {
      jest.runAllTimers()
    })
    const urlCells = await screen.findAllByText(BASE_GITHUB_URL, {
      exact: false,
    })
    for (const cell of urlCells) {
      const entryName = cell.parentElement!.firstChild!
      const entryIsStarred = cell.parentElement!.lastChild!
      const starButton = entryIsStarred.firstChild!
      if (starButton.textContent === 'star-empty') {
        const original = LocalStorageApi.getVariable(
          `repo-${entryName.textContent}`
        )
        // eslint-disable-next-line jest/no-conditional-expect
        expect(original ? original.isStarred : original).toBeFalsy()
        fireEvent(starButton, new MouseEvent('click', { bubbles: true }))
        const updated = LocalStorageApi.getVariable(
          `repo-${entryName.textContent}`
        )
        // eslint-disable-next-line jest/no-conditional-expect
        expect(updated.isStarred).toBe(true)
        LocalStorageApi.deleteVariable(`repo-${entryName.textContent}`)
        break
      }
    }
  })
})
