import Axios from 'axios'
import React from 'react'
import { GithubApi } from '../lib/services'
import Repositories from './repositories'
import RepositoriesContextProvider from './shared/contexts/repositories'
import RepositoriesFiltersContextProvider from './shared/contexts/repositories-filters'

// initialize GitHub service
GithubApi.set(Axios.create({ baseURL: 'https://api.github.com' }))

const App = () => {
  return (
    <RepositoriesContextProvider>
      <RepositoriesFiltersContextProvider>
        <Repositories />
      </RepositoriesFiltersContextProvider>
    </RepositoriesContextProvider>
  )
}

export default App
