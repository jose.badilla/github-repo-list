import React, {
  createContext,
  FunctionComponent,
  useMemo,
  useState,
} from 'react'
import { RepositoryTabs } from '../enums/current-repo-tab'

interface IRepositoriesFiltersContextProps {
  currentTab: RepositoryTabs
  changeCurrentTab: (tab: RepositoryTabs) => void
  filterByLanguage?: string
  changeFilterByLanguage: (language?: string) => void
}

export const RepositoriesFiltersContext: React.Context<IRepositoriesFiltersContextProps> = createContext<
  IRepositoriesFiltersContextProps
>({
  currentTab: RepositoryTabs.All,
  changeCurrentTab: () => undefined,
  filterByLanguage: undefined,
  changeFilterByLanguage: () => undefined,
})

const RepositoriesFiltersContextProvider: FunctionComponent = ({
  children,
}) => {
  const [currentTab, setCurrentTab] = useState<RepositoryTabs>(
    RepositoryTabs.All
  )
  const [filterByLanguage, setFilterByLanguage] = useState<string>()

  const contextValue = useMemo(
    () => ({
      currentTab,
      changeCurrentTab: (tab: RepositoryTabs) => setCurrentTab(tab),
      filterByLanguage,
      changeFilterByLanguage: (language?: string) =>
        setFilterByLanguage(language),
    }),
    [currentTab, filterByLanguage]
  )

  return (
    <RepositoriesFiltersContext.Provider value={contextValue}>
      {children}
    </RepositoriesFiltersContext.Provider>
  )
}

export default RepositoriesFiltersContextProvider
