import React, {
  createContext,
  FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from 'react'
import { RepositoriesService } from '../../../lib/services/repositories'
import { GithubRepository } from '../classes/github-repository'

interface IRepositoriesContextProps {
  repositories: GithubRepository[]
  updateRepositories: (updated: GithubRepository[]) => void
  isLoadingRepositories: boolean
}

export const RepositoriesContext: React.Context<IRepositoriesContextProps> = createContext<
  IRepositoriesContextProps
>({
  repositories: [],
  updateRepositories: () => undefined,
  isLoadingRepositories: false,
})

const RepositoriesContextProvider: FunctionComponent = ({ children }) => {
  const [repositories, setRepositories] = useState<GithubRepository[]>([])
  const [isLoadingRepositories, setIsLoadingRepositories] = useState<boolean>(
    true
  )

  const loadRepositories = useCallback(async () => {
    const repos = await RepositoriesService.listMostPopular()
    if (repos) {
      setRepositories(repos)
    }
  }, [])

  useEffect(() => {
    setIsLoadingRepositories(true)
    loadRepositories().then(() => setIsLoadingRepositories(false))
    return () => {
      setRepositories([])
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const contextValue = useMemo(
    () => ({
      repositories,
      updateRepositories: (updated: GithubRepository[]) =>
        setRepositories(updated),
      isLoadingRepositories,
    }),
    [repositories, isLoadingRepositories]
  )

  return (
    <RepositoriesContext.Provider value={contextValue}>
      {children}
    </RepositoriesContext.Provider>
  )
}

export default RepositoriesContextProvider
