import { Button, MenuDivider, MenuItem } from '@blueprintjs/core'
import { Select } from '@blueprintjs/select'
import React, { useCallback, useMemo } from 'react'

enum SelectSpecialType {
  None = 'e9d835f0-0979-4fb0-bd7b-328690448f84',
}

interface IStringSelectProps {
  items: string[]
  onItemSelect?: (item?: string) => void
  selected?: string
  hasNoneOption?: boolean
}

const StringSelect = ({
  items,
  onItemSelect = () => undefined,
  selected,
  hasNoneOption = false,
}: IStringSelectProps) => {
  const allItems = useMemo(() => {
    return hasNoneOption
      ? [SelectSpecialType.None as string].concat(items)
      : items
  }, [items, hasNoneOption])

  const handleItemSelect = useCallback(
    (item) =>
      item !== SelectSpecialType.None
        ? onItemSelect(item)
        : onItemSelect(undefined),
    [onItemSelect]
  )

  const matchItem = useCallback(
    (query, item) => item.toLowerCase().includes(query.toLowerCase()),
    []
  )

  const renderItem = useCallback(
    (item, itemProps) => {
      const isNone = hasNoneOption && item === SelectSpecialType.None
      const itemText = isNone ? 'None' : item
      const menuItem = (
        <MenuItem
          text={itemText}
          icon={
            (isNone ? !selected : selected === item) ? 'small-tick' : 'blank'
          }
          onClick={itemProps.handleClick}
          active={itemProps.modifiers.active}
        />
      )
      return (
        <React.Fragment key={itemText}>
          {menuItem}
          {isNone && <MenuDivider />}
        </React.Fragment>
      )
    },
    [selected, hasNoneOption]
  )

  return (
    <Select<string>
      items={allItems}
      onItemSelect={handleItemSelect}
      itemPredicate={matchItem}
      itemRenderer={renderItem}
      resetOnSelect={true}
    >
      <Button
        text={selected || 'Select...'}
        rightIcon="double-caret-vertical"
      />
    </Select>
  )
}

export default StringSelect
