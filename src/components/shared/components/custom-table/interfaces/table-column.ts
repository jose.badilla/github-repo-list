import { CSSProperties, ReactNode } from 'react'

export interface ICustomTableColumn<T> {
  id: string
  name: string
  getValue: (row: T) => ReactNode
  headerStyle?: CSSProperties
  rowStyle?: CSSProperties
}
