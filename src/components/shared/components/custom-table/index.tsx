import { HTMLTable, IHTMLTableProps } from '@blueprintjs/core'
import React from 'react'
import { ICustomTableColumn } from './interfaces/table-column'
import './styles/index.sass'

interface ICustomTableProps<T> extends IHTMLTableProps {
  entries: T[]
  columns: ICustomTableColumn<T>[]
  isHeaderSticky?: boolean
}

// tslint:disable-next-line: prettier
const CustomTable = <T,>({ entries, columns, isHeaderSticky = true, ...blueprintProps }: ICustomTableProps<T>) => {
  return (
    <HTMLTable
      className="w-full"
      condensed={true}
      striped={true}
      {...blueprintProps}
    >
      <thead>
        <tr>
          {columns.map((column) => (
            <th
              key={column.id}
              style={column.headerStyle || { height: '50px' }}
              className={
                isHeaderSticky ? 'custom-table-sticky-header' : undefined
              }
            >
              {column.name}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {entries.map((entry, rowIndex) => {
          return (
            <tr key={`row-${rowIndex}`}>
              {columns.map((column, colIndex) => (
                <td
                  key={`cell-${rowIndex}-${colIndex}`}
                  style={column.rowStyle}
                >
                  {column.getValue(entry)}
                </td>
              ))}
            </tr>
          )
        })}
      </tbody>
    </HTMLTable>
  )
}

export default CustomTable
