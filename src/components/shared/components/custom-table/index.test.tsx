import React from 'react'
import { render, screen } from '@testing-library/react'
import RepositoriesScaffold from './scaffold/repositories'
import ColumnsScaffold from './scaffold/columns'
import CustomTable from '.'
import { ICustomTableColumn } from './interfaces/table-column'
import { GithubRepository } from '../../classes/github-repository'

const tableEntries: GithubRepository[] = RepositoriesScaffold
const tableColumns = ColumnsScaffold as ICustomTableColumn<GithubRepository>[]

describe('custom table', () => {
  test('renders table with headers', async () => {
    render(<CustomTable entries={tableEntries} columns={tableColumns} />)
    let cell = await screen.findByText(tableColumns[0].name)
    tableColumns.forEach((column, i) => {
      expect(cell.textContent).toBe(column.name)
      if (i < tableColumns.length - 1) {
        cell = cell.nextSibling as HTMLElement
      }
    })
  })

  test('renders table with every entry', async () => {
    render(<CustomTable entries={tableEntries} columns={tableColumns} />)
    const cells = await screen.findAllByText('www.somerandomsite.com', {
      exact: false,
    })
    cells.forEach((cell, i) => {
      expect(cell.closest('tr')).toBeDefined()
      expect(cell.closest('tr')!.firstChild).toBeDefined()
      expect(cell.closest('tr')!.firstChild!.textContent).toBe(
        tableEntries[i].name
      )
    })
  })
})
