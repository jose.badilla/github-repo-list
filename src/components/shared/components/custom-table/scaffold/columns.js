import { Button } from "@blueprintjs/core"

const ColumnsScaffold = [
  {
    id: "name",
    name: "Name",
    getValue: (repo) => repo.name,
  },
  {
    id: 'htmlUrl',
    name: 'GitHub URL',
    getValue: (repo) => repo.htmlUrl,
  },
  {
    id: 'description',
    name: 'Description',
    getValue: (repo) => repo.description,
  },
  {
    id: 'numStars',
    name: 'Number of Stars',
    getValue: (repo) => repo.numStars,
  },
  {
    id: 'isStarred',
    name: 'Starred?',
    getValue: (repo) => {
      return (
        <Button
          icon={repo.isStarred ? 'star' : 'star-empty'}
          minimal={true}
          onClick={() => undefined}
        />
      )
    },
    rowStyle: { textAlign: 'center' },
  },
]

export default ColumnsScaffold