const RepositoriesScaffold = [{
  id: 'repository-a',
  name: 'Repository A',
  htmlUrl: 'www.somerandomsite.com/repo-a',
  description: 'A nice repo',
  numStars: 3,
  isStarred: true,
  language: 'Python',
}, {
  id: 'repository-b',
  name: 'Repository B',
  htmlUrl: 'www.somerandomsite.com/repo-b',
  description: 'A better repo',
  numStars: 33,
  isStarred: false,
  language: 'JavaScript',
}, {
  id: 'repository-c',
  name: 'Repository C',
  htmlUrl: 'www.somerandomsite.com/repo-c',
  description: 'An OK repo',
  numStars: 20,
  isStarred: true,
  language: 'C++',
}, {
  id: 'repository-d',
  name: 'Repository D',
  htmlUrl: 'www.somerandomsite.com/repo-d',
  description: 'Just a repo',
  numStars: 0,
  isStarred: false,
  language: 'PHP',
}, {
  id: 'repository-e',
  name: 'Repository E',
  htmlUrl: 'www.somerandomsite.com/repo-e',
  description: 'Ma Repo',
  numStars: 0,
  isStarred: false,
  language: 'JavaScript',
}]

export default RepositoriesScaffold
