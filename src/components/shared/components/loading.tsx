import { NonIdealState, Spinner } from '@blueprintjs/core'
import React from 'react'
const LOADING_CONTAINER_STYLE = { alignContent: 'center' }

const Loading = () => {
  return (
    <div className="h-full" style={LOADING_CONTAINER_STYLE}>
      <NonIdealState
        description="Loading repositories..."
        icon={<Spinner intent="primary" size={Spinner.SIZE_STANDARD} />}
      />
    </div>
  )
}

export default Loading
