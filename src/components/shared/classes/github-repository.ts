import { IsBoolean, IsInt, IsString, IsUrl } from 'class-validator'
import { IRawGithubRepository } from '../../../lib/services/repositories/model/get-repositories-response'

export class GithubRepository {
  @IsString()
  public id: string

  @IsString()
  public name: string

  @IsString()
  @IsUrl()
  public htmlUrl: string

  @IsString()
  public description: string

  @IsInt()
  public numStars: number

  @IsBoolean()
  public isStarred: boolean

  @IsString()
  public language: string

  constructor(rawGithubRepo: IRawGithubRepository) {
    this.id = rawGithubRepo.id
    this.name = rawGithubRepo.name
    this.htmlUrl = rawGithubRepo.html_url
    this.description = rawGithubRepo.description
    this.numStars = rawGithubRepo.stargazers_count
    this.isStarred = false
    this.language = rawGithubRepo.language
  }
}
