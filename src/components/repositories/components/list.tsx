import { Button, Card } from '@blueprintjs/core'
import React, { CSSProperties, useMemo } from 'react'
import { GithubRepository } from '../../shared/classes/github-repository'
import CustomTable from '../../shared/components/custom-table'
import { ICustomTableColumn } from '../../shared/components/custom-table/interfaces/table-column'
const DEFAULT_TABLE_CONTAINER_STYLE = {
  height: '86%',
  overflow: 'auto',
  margin: '2%',
  padding: 0,
}

interface IRepositoriesListProps {
  repositories: GithubRepository[]
  onRepositoryStarred?: (
    repository: GithubRepository,
    isStarred: boolean
  ) => void
  tableContainerStyle?: CSSProperties
}

const RepositoriesList = ({
  repositories,
  onRepositoryStarred: handleRepositoryStarred = () => undefined,
  tableContainerStyle = DEFAULT_TABLE_CONTAINER_STYLE,
}: IRepositoriesListProps) => {
  const tableColumns: ICustomTableColumn<GithubRepository>[] = useMemo(() => {
    return [
      {
        id: 'name',
        name: 'Name',
        getValue: (repo) => repo.name,
      },
      {
        id: 'url',
        name: 'GitHub URL',
        getValue: (repo) => repo.htmlUrl,
      },
      {
        id: 'description',
        name: 'Description',
        getValue: (repo) => repo.description,
      },
      {
        id: 'language',
        name: 'Language',
        getValue: (repo) => repo.language,
      },
      {
        id: 'numStars',
        name: 'Number of Stars',
        getValue: (repo) => repo.numStars,
      },
      {
        id: 'isStarred',
        name: 'Starred?',
        getValue: (repo) => {
          return (
            <Button
              icon={repo.isStarred ? 'star' : 'star-empty'}
              minimal={true}
              onClick={() => handleRepositoryStarred(repo, !repo.isStarred)}
            />
          )
        },
        rowStyle: { textAlign: 'center' },
      },
    ]
  }, [handleRepositoryStarred])

  return (
    <Card style={tableContainerStyle}>
      <CustomTable<GithubRepository>
        entries={repositories}
        columns={tableColumns}
      />
    </Card>
  )
}

export default RepositoriesList
