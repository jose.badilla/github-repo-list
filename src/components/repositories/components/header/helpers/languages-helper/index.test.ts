import LanguagesHelper from '.'
import RepositoriesScaffold from '../../../../../shared/components/custom-table/scaffold/repositories'

describe('language helper', () => {
  test('languages helper retrieves languages from repos', () => {
    const languages = LanguagesHelper.getLanguagesFromRepositories(
      RepositoriesScaffold
    )
    expect(languages.length).toEqual(4)
  })
})
