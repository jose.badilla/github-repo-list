import { GithubRepository } from '../../../../../shared/classes/github-repository'

const getLanguagesFromRepositories = (repositories: GithubRepository[]) => {
  return repositories.reduce((languages: string[], repo: GithubRepository) => {
    const found = languages.find((lang) => repo.language === lang)
    if (!found) {
      return languages.concat([repo.language])
    }
    return languages.filter((lang) => lang !== null)
  }, [])
}

const LanguagesHelper = {
  getLanguagesFromRepositories,
}

export default LanguagesHelper
