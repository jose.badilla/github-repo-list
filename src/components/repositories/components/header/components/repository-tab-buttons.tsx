import {
  AnchorButton,
  Button,
  ButtonGroup,
  Tag,
  Tooltip,
} from '@blueprintjs/core'
import React from 'react'
import { RepositoryTabs } from '../../../../shared/enums/current-repo-tab'

interface IRepositoryTabButtonsProps {
  currentTab: RepositoryTabs
  onCurrentTabChange: (tab: RepositoryTabs) => void
  numStarredRepositories: number
  disabledStarredTabTooltip?: string
}

const RepositoryTabButtons = ({
  currentTab,
  onCurrentTabChange,
  numStarredRepositories,
  disabledStarredTabTooltip,
}: IRepositoryTabButtonsProps) => {
  return (
    <ButtonGroup>
      <Button
        text="All Repositories"
        active={currentTab === RepositoryTabs.All}
        onClick={() => onCurrentTabChange(RepositoryTabs.All)}
      />
      <Tooltip
        disabled={numStarredRepositories > 0}
        content={disabledStarredTabTooltip}
        boundary="window"
      >
        <AnchorButton
          text={
            <span>
              Starred Repositories
              <Tag round={true} style={{ marginLeft: '5px' }}>
                {numStarredRepositories}
              </Tag>
            </span>
          }
          active={currentTab === RepositoryTabs.Starred}
          onClick={() => onCurrentTabChange(RepositoryTabs.Starred)}
          disabled={numStarredRepositories === 0}
        />
      </Tooltip>
    </ButtonGroup>
  )
}

export default RepositoryTabButtons
