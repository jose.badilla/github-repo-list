import React, { useContext, useEffect, useMemo, useState } from 'react'
import {
  FormGroup,
  Navbar,
  NavbarDivider,
  NavbarGroup,
} from '@blueprintjs/core'
import { RepositoryTabs } from '../../../shared/enums/current-repo-tab'
import { RepositoriesContext } from '../../../shared/contexts/repositories'
import StringSelect from '../../../shared/components/string-select'
import LanguagesHelper from './helpers/languages-helper'
import { RepositoriesFiltersContext } from '../../../shared/contexts/repositories-filters'
import FilterRepositoriesHelper from '../../helpers/filter-repositories'
import { GithubRepository } from '../../../shared/classes/github-repository'
import RepositoryTabButtons from './components/repository-tab-buttons'

const RepositoriesHeader = () => {
  const { repositories } = useContext(RepositoriesContext)
  const {
    currentTab,
    changeCurrentTab,
    filterByLanguage,
    changeFilterByLanguage,
  } = useContext(RepositoriesFiltersContext)

  const [starredRepositories, setStarredRepositories] = useState<
    GithubRepository[]
  >([])

  const languages = useMemo(() => {
    return LanguagesHelper.getLanguagesFromRepositories(repositories)
  }, [repositories])

  useEffect(() => {
    const starredRepos = FilterRepositoriesHelper.filterBy(repositories, {
      language: filterByLanguage,
      onlyStarred: true,
    })
    setStarredRepositories(starredRepos)
    if (starredRepos.length === 0 && currentTab === RepositoryTabs.Starred) {
      changeCurrentTab(RepositoryTabs.All)
    }
  }, [repositories, filterByLanguage]) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Navbar>
      <NavbarGroup align="left">
        <RepositoryTabButtons
          currentTab={currentTab}
          onCurrentTabChange={(tab) => changeCurrentTab(tab)}
          numStarredRepositories={starredRepositories.length}
          disabledStarredTabTooltip={`You have no starred repositories${
            filterByLanguage ? ` (for ${filterByLanguage})` : ''
          }`}
        />
        <NavbarDivider />
        <FormGroup
          label="Filter by language:"
          inline={true}
          style={{ marginBottom: 0 }}
        >
          <StringSelect
            items={languages}
            selected={filterByLanguage}
            onItemSelect={changeFilterByLanguage}
            hasNoneOption={true}
          />
        </FormGroup>
      </NavbarGroup>
    </Navbar>
  )
}

export default RepositoriesHeader
