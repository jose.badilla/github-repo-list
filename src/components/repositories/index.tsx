import React, { useCallback, useContext, useEffect, useState } from 'react'
import { RepositoriesService } from '../../lib/services/repositories'
import { GithubRepository } from '../shared/classes/github-repository'
import Loading from '../shared/components/loading'
import { RepositoriesContext } from '../shared/contexts/repositories'
import { RepositoriesFiltersContext } from '../shared/contexts/repositories-filters'
import { RepositoryTabs } from '../shared/enums/current-repo-tab'
import RepositoriesHeader from './components/header'
import RepositoriesList from './components/list'
import FilterRepositoriesHelper from './helpers/filter-repositories'

const Repositories = () => {
  const {
    repositories,
    updateRepositories,
    isLoadingRepositories,
  } = useContext(RepositoriesContext)
  const { currentTab, filterByLanguage } = useContext(
    RepositoriesFiltersContext
  )

  const [filteredRepositories, setFilteredRepositories] = useState<
    GithubRepository[]
  >([])

  const handleRepositoryStarred = useCallback(
    async (repository: GithubRepository, isStarred: boolean) => {
      const update = RepositoriesService.updateOne(repository.name, {
        isStarred,
      })
      updateRepositories(
        repositories.map((repo) =>
          repo.id === repository.id ? { ...repo, ...update } : repo
        )
      )
    },
    [repositories, updateRepositories]
  )

  const refreshFilteredRepositories = useCallback(() => {
    if (repositories.length > 0) {
      const filteredRepos = FilterRepositoriesHelper.filterBy(repositories, {
        onlyStarred: currentTab === RepositoryTabs.Starred,
        language: filterByLanguage,
      })
      if (filteredRepos.length === 0) {
        setFilteredRepositories(repositories.slice())
      } else {
        setFilteredRepositories(filteredRepos)
      }
    }
  }, [repositories, currentTab, filterByLanguage]) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    refreshFilteredRepositories()
  }, [refreshFilteredRepositories])

  if (isLoadingRepositories) {
    return <Loading />
  }

  return (
    <div className="h-full">
      <RepositoriesHeader />
      <div className="h-full">
        <RepositoriesList
          repositories={filteredRepositories}
          onRepositoryStarred={handleRepositoryStarred}
        />
      </div>
    </div>
  )
}

export default Repositories
