import FilterRepositoriesHelper from '.'
import RepositoriesScaffold from '../../../shared/components/custom-table/scaffold/repositories'

describe('filter repositories helper', () => {
  test('filters by language', () => {
    const filteredRepos = FilterRepositoriesHelper.filterBy(
      RepositoriesScaffold,
      { language: 'Python' }
    )
    expect(RepositoriesScaffold.length).toBeGreaterThan(filteredRepos.length)
    expect(filteredRepos.length).toEqual(1)
    expect(filteredRepos[0].language).toEqual('Python')
  })

  test('filters by starred', () => {
    const filteredRepos = FilterRepositoriesHelper.filterBy(
      RepositoriesScaffold,
      { onlyStarred: true }
    )
    expect(RepositoriesScaffold.length).toBeGreaterThan(filteredRepos.length)
    expect(filteredRepos.length).toEqual(2)
    expect(filteredRepos[0].isStarred).toEqual(true)
  })

  test('filters by starred and language', () => {
    const filteredRepos = FilterRepositoriesHelper.filterBy(
      RepositoriesScaffold,
      { onlyStarred: true, language: 'Python' }
    )
    expect(RepositoriesScaffold.length).toBeGreaterThan(filteredRepos.length)
    expect(filteredRepos.length).toEqual(1)
    expect(filteredRepos[0].isStarred).toEqual(true)
    expect(filteredRepos[0].language).toEqual('Python')
  })
})
