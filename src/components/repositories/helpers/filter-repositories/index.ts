import { GithubRepository } from '../../../shared/classes/github-repository'

const filterByLanguage = (
  repositories: GithubRepository[],
  language: string
) => {
  return language
    ? repositories.filter((r) => r.language === language)
    : repositories.slice()
}

interface IRepositoryFilters {
  onlyStarred?: boolean
  language?: string
}
const filterBy = (
  repositories: GithubRepository[],
  { onlyStarred = false, language }: IRepositoryFilters
) => {
  let filteredRepositories: GithubRepository[] = repositories
  if (language) {
    filteredRepositories = filterByLanguage(repositories, language)
  }
  if (onlyStarred) {
    filteredRepositories = filteredRepositories.filter((repo) => repo.isStarred)
  }
  return filteredRepositories
}

const FilterRepositoriesHelper = {
  filterBy,
}

export default FilterRepositoriesHelper
