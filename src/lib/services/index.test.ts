import { LocalStorageApi } from '.'

const storageKey = 'foo'

describe('services', () => {
  afterAll(() => {
    LocalStorageApi.deleteVariable(storageKey)
  })
  test('stores information in local storage correctly', () => {
    LocalStorageApi.setVariable(storageKey, { value: 10 })
    expect(LocalStorageApi.getVariable(storageKey)).toEqual({
      value: 10,
    })
  })

  test('removes information from local storage correctly', () => {
    LocalStorageApi.setVariable(storageKey, { value: 10 })
    LocalStorageApi.deleteVariable(storageKey)
    expect(LocalStorageApi.getVariable(storageKey)).toEqual(undefined)
  })
})
