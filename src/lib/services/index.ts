import { AxiosInstance } from 'axios'

let githubApi: AxiosInstance

export const GithubApi = {
  get: () => githubApi,
  set: (apiInstance: AxiosInstance) => (githubApi = apiInstance),
}

export const LocalStorageApi = {
  getVariable: (key: string, { defaultValue }: { defaultValue?: any } = {}) => {
    const value = localStorage.getItem(key)
    return value ? JSON.parse(value) : defaultValue
  },
  setVariable: (key: string, value: any) => {
    if (value === undefined) {
      return
    }
    localStorage.setItem(key, JSON.stringify(value))
  },
  deleteVariable: (key: string) => {
    localStorage.removeItem(key)
  },
}
