import { IsArray, IsBoolean, IsInt } from 'class-validator'
import { GithubRepository } from '../../../../components/shared/classes/github-repository'

export interface IRawGithubRepository {
  id: string
  name: string
  html_url: string
  description: string
  stargazers_count: number
  language: string
}

interface IGetRepositoriesResponse {
  total_count: number
  incomplete_results: boolean
  items: IRawGithubRepository[]
}

export class GetRepositoriesResponse {
  @IsInt()
  public totalCount: number

  @IsBoolean()
  public incompleteResults: boolean

  @IsArray()
  public items: GithubRepository[]

  constructor(response: IGetRepositoriesResponse) {
    this.totalCount = response.total_count
    this.incompleteResults = response.incomplete_results
    this.items = response.items.map((item) => new GithubRepository(item))
  }
}
