import Axios from 'axios'
import { RepositoriesService } from '.'
import { GithubApi } from '..'

GithubApi.set(Axios.create({ baseURL: 'https://api.github.com' }))

describe('repositories service', () => {
  jest.retryTimes(2)
  test('lists most popular repositories', async () => {
    const repos = await RepositoriesService.listMostPopular()
    expect(repos !== undefined && repos.length > 0).toBe(true)
  })
})
