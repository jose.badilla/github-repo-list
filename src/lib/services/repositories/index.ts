import { AxiosRequestConfig } from 'axios'
import { GithubApi, LocalStorageApi } from '..'
import { GithubRepository } from '../../../components/shared/classes/github-repository'
import { GetRepositoriesResponse } from './model/get-repositories-response'

interface IUpdateOneRepository {
  isStarred: boolean
}

export const RepositoriesService = {
  listMostPopular: async (
    config?: AxiosRequestConfig
  ): Promise<GithubRepository[] | undefined> =>
    GithubApi.get()
      .get(
        '/search/repositories?q=created:>2017-01-10&sort=stars&order=desc',
        config
      )
      .then((response) => {
        if (response.status === 200) {
          const repos = new GetRepositoriesResponse(response.data).items
          return repos.map((repo) => {
            const localStorage = LocalStorageApi.getVariable(
              `repo-${repo.name}`,
              {
                defaultValue: false,
              }
            )
            return { ...repo, ...localStorage }
          })
        }
      }),
  updateOne: (
    id: string,
    data: IUpdateOneRepository
  ): IUpdateOneRepository | undefined => {
    LocalStorageApi.setVariable(`repo-${id}`, data)
    return LocalStorageApi.getVariable(`repo-${id}`)
  },
}
